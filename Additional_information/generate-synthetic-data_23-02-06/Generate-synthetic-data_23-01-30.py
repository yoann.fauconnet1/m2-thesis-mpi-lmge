'''
##Libra#####
'''

import numpy as np ; import re ; import random
from Bio.Align import substitution_matrices ; from collections import defaultdict

'''
##Tools#####
'''
	
#Translate a given nucleotide sequence into the corresponding amino acid sequence
def translate(seq):
	table = {
		'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
		'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
		'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
		'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',				
		'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
		'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
		'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
		'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
		'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
		'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
		'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
		'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
		'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
		'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
		'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
		'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W',
	}
	protein =""
	if len(seq)%3 == 0:
		for i in range(0, len(seq), 3):
			codon = seq[i:i + 3]
			protein+= table[codon]
	return protein


#Give the DeltaS of each amino acid of a contig compared to the genome 
#(In LL, DeltaS is calculated for the contig between genome in region matching wth an ORF)
def calc_delta_S(contig, startg, endg) :
	deltaS = list()
	contig_index = 0
	c_aa = translate(contig)
	for a in translate(genome[int(startg):int(endg)]) :                  
		if str(a) == str(c_aa[contig_index]) :
			deltaS.append(float(0.0))
		else:
			if c_aa[contig_index] == '*': #If the mutation produce a stop codon in this ORF, add -20 
				deltaS.append((float(blosum[a][c_aa[contig_index]])-float(blosum[a][a]))-20)
			else:
				deltaS.append(float(blosum[a][c_aa[contig_index]])-float(blosum[a][a]))	
		contig_index += 1
	return deltaS


#Give kronecker index for xi and y
def kronecker(xi,y):
	if xi==y:
		k=1
	else:
		k=0
	return k


#Calculate the pniy for a specific position xi and nucleotide y
def calc_pniy(xi, y, xi_pos):	
	if xi == y :
		final = 1-beta
	else:
		k = kronecker(xi,y)
		pniy = beta/3

		prod_DeltaS = 1
		for o in orfs :
			deltas = 0.0
			if orfs[o][0] <= xi_pos <= orfs[o][1] :
				xo = xi_pos - orfs[o][0]
				orf_seq = genome[orfs[o][0]:orfs[o][1]+1] #+1 or not ?
				temp = orf_seq[:xo]+y+orf_seq[xo+1:] #We're going over all ORF pos matching with xi, we replace the nt in orf by y and calculate deltas with genome
				if   xo%3 == 0 :
					codon_orf = translate(temp[xo:xo+3])
					codon_ref = translate(genome[ (orfs[o][0]+xo) : (orfs[o][0]+xo+3) ])
				elif xo%3 == 1 :
					codon_orf = translate(temp[xo-1:xo+2]) #And not +1 cause the [] is exclusive for the last boy
					codon_ref = translate(genome[ (orfs[o][0]+xo-1) : (orfs[o][0]+xo+2) ])
				elif xo%3 == 2 :
					codon_orf = translate(temp[xo-2:xo+1])
					codon_ref = translate(genome[ (orfs[o][0]+xo-2) : (orfs[o][0]+xo+1) ])
				#I can calculate deltaS here (for forward ORF only)
				if codon_orf == '*' :
					deltas = float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref])
					deltas -= 20.0
				elif codon_orf == codon_ref :
					deltas = 0.0	
				else:
					deltas = float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref])
			exp_deltas = np.exp(alpha*deltas)	
			prod_DeltaS *= exp_deltas
		#print('eee', pniy, prod_DeltaS)
		
		final = pniy*prod_DeltaS
	return final #At the end, if there is no ORF corresponding to this position in the ORF, return 1.			

#mn = mutation number on the read, read = nucleotidic sequence, i_n = contig start position, l = read length, alpha, beta
def calc_log_likelihood(mn, read, orfs, i_n, l, alpha, beta):
	Pyn = 0
	first = mn*np.log(beta/3)+(len(read)-mn)*np.log(1-beta) #0 mut in the first turn, mn increase if a mut is accepted
	second = 0
	for orf in orfs : #Forward ORFs
		if i_n < orfs[orf][0] < i_n+l or i_n < orfs[orf][1] < i_n+l :
			c_on_o = read[orfs[orf][0]-i_n:orfs[orf][0]-i_n+(orfs[orf][0]-orfs[orf][1])] #Part of the contig matching with current ORF
			second += alpha*np.sum(calc_delta_S(c_on_o, orfs[orf][0], orfs[orf][0]+len(c_on_o))) #Sum of ag * Sum of deltaS at each positions	                   
	third=0
	xi_pos = i_n
	for xi in read : #first sum after the minus in the formula (Product for each contig of)
		pniy = 0
		li = ['A','T','C','G']
		#print('S')
		for y in li :
			pniy += calc_pniy(xi,y,xi_pos)
			#print('pniy', pniy)
		third += np.log(pniy) #Sum of log(Sum of pniy for 3nt) calculate for each nt of the contig
		xi_pos += 1
		#print('E')
	print(first, second, third)	
	Pyn = first+second-third
	print('The log likelihod value is :', Pyn)	
	return Pyn


'''
##Variable Setup#####
'''

#Load the BLOSUM substitution matrix
blosum = substitution_matrices.load('BLOSUM62') #print(blosum['M']['F']) --> Way to call an aa-aa score

#Generate data 
#I use a real genome : Bacillus phage GIL160 and accession : 000859725 to make synthetic datas (there is 3 n and I replace them by A to avoid error, take care of this biais)
#genome = random_dna(3000)
genome = ''
with open ('data/Bacillus-phage-clean.fa','r') as f1 :
	for l in f1 : 
		if not l.startswith('>') :
			l = l.strip().replace('n','A')
			genome += l

#getORF part on the genome to have a dict with ORFname, start and end on the reference genome as a list : ORF = [51, 90]
#First : use the following command in bash --> getorf data/GCA_000859725.1.txt -o data/GCA_000859725.1_ORFs.fa
#In results, we can see ORFs on the reverse side, but let consider the forward ones. 

orfs = defaultdict(list)
with open('data/GCA_000859725.1_ORFs.fa','r') as f1 : 
	for l in f1 : 
		l=l.strip()
		if not re.search('REVERSE',l) and l.startswith('>'):
			lsplit = l.split()
			orfs[str(lsplit[0].lstrip('>'))] = [int(lsplit[1].lstrip('[')),int(lsplit[3].rstrip(']'))]

beta = np.random.uniform(0.90,1.00) 
alpha = np.random.random() #Between 0 and 1
print('The random beta value for this run is', beta)
print('The random alpha value for this run is', alpha)
 
l = 100  #read length base on Illumina sequencing
N = 1 #Number of reads (small number to be faster)
read_list = list()


'''
Code#####
'''

#This part produce a read, try to mutate the read, N times to produce a list of read based on a reference genome
for yn in range(N) :
	i_n = round(np.random.uniform(0,len(genome)-l))             #a random position in genome (if I had +1 read can have a length of l-1)     
	read = genome[i_n:i_n+l]                                    #a length l read starting on i_n
	r = genome[i_n:i_n+l] 
	read_index = np.random.permutation(l)                       #a list storing permuted nucleotide index
	mut_index = np.arange(round(beta*l))                        #a list of index with a length equal to the possible mutations number
	mn = 0
	for m in mut_index :                                        #for each mutation index (m) :
		mn_mut = mn+1
		#print('m value :', m, 'index of the nucleotide to mutate :', read_index[m])
		a = ['A','T','C','G'] ; a.remove(read[read_index[m]])
		yn_mutated = list(read)
		#print('Old nt', yn_mutated[read_index[m]])
		yn_mutated[read_index[m]] = random.choice(a)
		#print('New nt', yn_mutated[read_index[m]])
		yn_mutated = ''.join(yn_mutated)

		Pyn = calc_log_likelihood(mn, read, orfs, i_n, l, alpha, beta)
		Pyn_mut = calc_log_likelihood(mn_mut, yn_mutated, orfs, i_n, l, alpha, beta)
		print('Final values for this read of Pyn and Pyn_mut', Pyn, Pyn_mut)
		print(f'On read {yn} and for mutation {m} the ratio is {Pyn_mut/Pyn}')
		
		if np.random.random() < (Pyn_mut/Pyn) : #Here, 25% of mutations are allowed in the read
			mn += 1
			read = yn_mutated
	read_list.append(read)
	print('\ninitial read :', r)
	print('mutate read :', yn_mutated, '\n')		

