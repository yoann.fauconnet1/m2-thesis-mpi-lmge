###################################THERE IS AN ERROR TO REPLACE WHEN FIXED############################################

'''
##Libra#####
'''

import numpy as np ; import random ; import math
from Bio.Align import substitution_matrices ; from collections import defaultdict

'''
##Tools#####
'''

#Give a configurable length reference genome
def random_dna(length):
	return ''.join(random.choice('ATCG') for _ in range(length))
	
#Translate a given nucleotide sequence into the corresponding amino acid sequence
def translate(seq):
	table = {
		'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
		'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
		'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
		'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',				
		'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
		'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
		'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
		'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
		'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
		'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
		'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
		'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
		'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
		'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
		'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
		'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W',
	}
	protein =""
	if len(seq)%3 == 0:
		for i in range(0, len(seq), 3):
			codon = seq[i:i + 3]
			protein+= table[codon]
	return protein


#Give the DeltaS of each amino acid of a contig compared to the variable ref_genome
def calc_delta_S(contig, startg, endg) :
	deltaS = list()
	contig_index = 0
	c_aa = translate(contig)
	for a in translate(ref_genome[int(startg):int(endg)]) :                  
		if str(a) == str(c_aa[contig_index]) :
			deltaS.append(float(0.0))
		else:
			if c_aa[contig_index] == '_': #If the mutation produce a stop codon in this ORF, add -20 
				deltaS.append((float(blosum[a][c_aa[contig_index]])-float(blosum[a][a]))-20)
			else:
				deltaS.append(float(blosum[a][c_aa[contig_index]])-float(blosum[a][a]))	
		contig_index += 1
	return deltaS


#Give kronecker index for xi and y
def kronecker(xi,y):
	if xi==y:
		k=1
	else:
		k=0
	return k

'''
#Compute Product(exp**(alphag*deltaS)) for each position of an ORF (atr) on the contig (c). Give then the product of this per-positions values. (This function use the variable ref_genome)
#It doesn't work if the ORF length is not a multiple of 3
def calc_delta_Spe(c, orf_names, ORF_seq, startg, endg) :
	product = 1.0
	contig_index = 0
	y1_tr = translate(ORF_seq)
	for a in translate(ref_genome[int(startg):int(endg)]) : 
		if str(a) == str(y1_tr[contig_index]) :
			product *= 1.0
		else:
			if y1_tr[contig_index] == '_' : #If the mutation produce a stop codon in this ORF, add -20 
				product *= np.exp(orf[c][orf_names][1]*(float(blosum[a][y1_tr[contig_index]])-float(blosum[a][a]))-20)
			else:
				product *= np.exp(orf[c][orf_names][1]*float(blosum[a][y1_tr[contig_index]])-float(blosum[a][a]))
		contig_index += 1
	return product
'''

#Give a pniy value for a contig considering each ORF on it
#c for contig name
'''
def calc_pniy(c,xi,y, xi_pos): 
	k = kronecker(xi,y)
	pniy = ((bn[c]/3)**(1-k))*(1-bn[c])**k
	
	sum_dg = 1
	#For each ORF in contig c 
	for atr in orf[c] : 
		#Compute Product of Product(exp**(alphag*deltaS)) #The first product give a value per ORF positions so we make another product to get only one value for the ORF
		sum_dg = sum_dg*calc_delta_Spe(c, atr, orf[c][atr][0], orf[c][atr][2], orf[c][atr][3])
		print('sum_dg', sum_dg) #Sum_dg est toujours egal a 1 ???
	pniy = pniy*sum_dg
	return pniy
'''

def calc_pniy(c, xi, y, xi_pos):
	struc = dict()
	k = kronecker(xi,y)
	pniy = ((bn[c]/3)**(1-k))*(1-bn[c])**k
	
	prod_DeltaS = 1
	for o in orf[c] :
		exp_deltas = 1
		if orf[c][o][2] < orf[c][o][3] :
			#print('\nORF start on genone :', orf[c][o][2], '\nORF end on genome :', orf[c][o][3], '\nxi position on genome :', xi_pos)
			if orf[c][o][2] <= xi_pos <= orf[c][o][3] :
				xo = xi_pos - orf[c][o][2]
				print('xo (position in the ORF) :', xo)
				if round((xo/3)%1*10) == 0 :
					codon_orf = translate(orf[c][o][0][xo:xo+3])
					codon_ref = translate(ref_genome[ (orf[c][o][2]+xo) : (orf[c][o][2]+xo+3) ])
				elif round((xo/3)%1*10) == 3 :
					codon_orf = translate(orf[c][o][0][xo-1:xo+2]) #And not +1 cause the [] is exclusive for the last boy
					codon_ref = translate(ref_genome[ (orf[c][o][2]+xo-1) : (orf[c][o][2]+xo+2) ])
				elif round((xo/3)%1*10) == 7 :
					codon_orf = translate(orf[c][o][0][xo-2:xo+1])
					codon_ref = translate(ref_genome[ (orf[c][o][2]+xo-2) : (orf[c][o][2]+xo+1) ])
					
				#I can calculate deltaS here (for forward ORF only)
				if codon_orf == '*' :
					exp_deltas = np.exp(orf[c][o][1]*(float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref]))-20)
				else:
					exp_deltas = np.exp(orf[c][o][1]*(float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref])))	
		else:
			if orf[c][o][3] <= xi_pos <= orf[c][o][2] :
				xo = orf[c][o][2] - xi_pos
				if round((xo/3)%1*10) == 0 :
					codon_orf = translate(orf[c][o][0][xo:xo+3])
					codon_ref = translate(ref_genome[ (orf[c][o][2]+xo) : (orf[c][o][2]+xo+3) ])
				elif round((xo/3)%1*10) == 3 :
					codon_orf = translate(orf[c][o][0][xo-1:xo+2])
					codon_ref = translate(ref_genome[ (orf[c][o][2]+xo-1) : (orf[c][o][2]+xo+2) ])
				elif round((xo/3)%1*10) == 7 :
					codon_orf = translate(orf[c][o][0][xo-2:xo+1])
					codon_ref = translate(ref_genome[ (orf[c][o][2]+xo-2) : (orf[c][o][2]+xo+1) ])
					
				#and put the second deltas calculator here (for reverse ORF only)
				if codon_orf == '*' :
					exp_deltas = np.exp(orf[c][o][1]*(float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref]))-20)
				else:
					exp_deltas = np.exp(orf[c][o][1]*(float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref])))				
		
		#At xi position, we make the product of exp(a*deltaS) for all concerned ORFs			
		prod_DeltaS = prod_DeltaS * exp_deltas
	final = pniy*prod_DeltaS	
	return final #At the end, if there is no ORF correspondinbg to this position in the ORF, return 1.			
				


#xi is nucleotide, find it position on the genome (start_contig+xi_indexInContig) ==< Give this infos to feed the function
#Iterate over all ORFs : 
	#if start_ORF+end_ORF > 0 :	
		#if start_ORF <= xi_pos <= end_ORF : #Use positions on genome
			#Find ORF_nt position (xo = xi_pos-start_ORF) (index in ORF)
	    		#if round((xo/3)%1*10) == 0 we are on the first reading frame 
				#Traduct the codon xo,xo+1,xo+2
	    			#Traduct ref_genome at this position
	    		#elif round((xo/3)%1*10) == 3 we are on the second reading frame 
	    			#Traduct the codon xo-1,xo,xo+1
	    			#Traduct ref_genome at this position
	    		#elif round((xo/3)%1*10) == 7 we are on the third reading frame
	    			#Traduct the codon xo,xo-1,xo-2
	    			#Traduct ref_genome at this position
	    		#calc_deltaSpe    			
	#else: #We are on on ORF in the reverse strand
		#if end_ORF <= xi_pos <= start_ORF : #Use positions on genome
			#Find ORF_nt position (xo = start_ORF-xi_pos)
	    		#if round((xo/3)%1*10) == 0 we are on the first reading frame 
				#Traduct the codon xo,xo+1,xo+2  #Genomic index are reversed but ORF index are in a regular order, so nothing to change here
	    			#Traduct ref_genome at this position #Take care cause this step is reverse!
	    		#elif round((xo/3)%1*10) == 3 we are on the second reading frame 
	    			#Traduct the codon xo-1,xo,xo+1
	    			#Traduct ref_genome at this position
	    		#elif round((xo/3)%1*10) == 7 we are on the third reading frame
	    			#Traduct the codon xo,xo-1,xo-2
	    			#Traduct ref_genome at this position
	    		#calc_deltaSpe    			
	#Product of exp(a*deltaSpe) for ORFs at this position					

  			
#Ok, we have xi, the contig nucleotide. We want to use it as an amino acid, there is 3 possible codons for xi : xi-X-X, X-xi-X, X-X-xi. 
#We can't use ORF reading frame cause it is different from an ORF to another and there is ORFs in the reverse strand. It's not possible to
#associate an amino acid to xi and compare it to an ORF amino acid at the same position, isn't it ?
#So let's imagine that the reading frame is defined by the start of the sequence (3 first bases are the full first codon), if the contig 
#and one or more orf are not on the same reading frame, we cannot compare them by aa cause codons are different.
#We have to adapt the contig translate sequence to the start of the ORF. What if 2 overlapping ORF are not on the same reading frame ? 
#...
#Approach with a dict considering amino acid of ORFs and the coresponding position in the amino acid sequence of a contig is unreliable
#Should we consider nucleotidic positions ? why not but how ?
#Check if there are ORFs nucleotides at the same position as xi. Then, we can convert in amino acid, but with which codon ? (xi-X-X, X-xi-X, X-X-xi)

#xi is from the contig, find corresponding y in one ORF, take the codon with y according to ORF reading frame. Then, compare the amino acid with the genomic one
#Then, find y in another contig and do the same thing. At the end of the loop, make the the product 


'''
##Test data production##### 
'''

#To test the first version of this algorithm, let's create a reference genome, with only one contig and two ORF on it. There is one mutation in the contig and one ORF get it.


#We want to have a static reference genome for now to compare several runs (we can change the number of mutations on the contig for example) so : 
ref_genome = 'TCCTTGGGACAGCTTCTGCGTGACTTTGCACCCAAAGTTCCACGGTCACATGCGCCTGAG'

#We also need a contig, let's take a length of 9 amino acids (27bp) and 1 SNP (at index 10 = C-->T)
yn = dict()
yn['y1'] = ['CAAAGTTCCATGGTCACATGCGCCTGA', 32, 59]

#Two fake ORF on the contig y1 with overlapping positions
#First ORF have a mutation compared to ref_genome but not the second one
orf = defaultdict(lambda: defaultdict(list))
                          #sequence,       ag, start_on_Genome, end_on_Genome
orf['y1']['ORF1'] = ['ATGGTCACATGCGCCTGA', 0.5, 41, 59]
orf['y1']['ORF2'] = ['CATGCGCCTGAG', 1, 48, 60]


#BWA for info about SNP, position in the aligment, ... 
#Be very aware when I'm going to change this into real variable, many change to do

#I want to associate 



'''
##Variable Setup#####
'''

#Load the BLOSUM substitution matrix
blosum = substitution_matrices.load('BLOSUM62') #print(blosum['M']['F']) --> Way to call an aa-aa score

#%identity between contig and genome in absence of selection (yes we can calculate the real b_n for contig)
#Only one contig to assess a beta value
bn = dict()	
for c in yn : 
	bn[c] = 0.95 
	
#nb of mutation in the contig relative to the genome (If 1 SNP, this value is equal to 1)
mn = 1


'''
##Compute the log likelihood#####
'''

#With classical loops (WARNING : In the formula, ag is for orf and not for contig, in a real case we need to make the sum of these elements and to replace the contig n by all ORFs)


LL = 0
for c in yn : #For each contigs
	first = mn*np.log(bn[c]/3)+(len(yn[c][0])-mn)*np.log(1-bn[c])
	second = 0
	for atr in orf[c] : #for each ORF in contig c         ORF Sequence    ORF start on g     ORF end on g
		second += (orf[c][atr][1]*np.sum(calc_delta_S(orf[c][atr][0], orf[c][atr][2], orf[c][atr][3]))) #Sum of ag * Sum of deltaS at each positions
	third=0
	xi_pos = yn[c][1]
	for xi in yn[c][0] : #first sum after the minus in the formula (Product for each contig of)
		pniy=0
		l = ['A','T','C','G']
		#l.remove(xi)                   #useless t consider nucleotide of the contig here, it's the genomic one as xi
		for y in l :
			pniy += calc_pniy(c,xi,y,xi_pos)
		third += np.log(pniy) #Sum of log(Sum of pniy for 3nt) calculate for each nt of the contig
		xi_pos += 1
	#contig_likelihood = first+second-third
	LL += first+second-third
	print(first, second, third)
print('The log likelihod value is :', LL)

'''
print('\nwork in progress')
new_f('y1','Y','D', 46)
print()

a = new_f('y1','Y','D', 46)
print(a)
'''

exit() 


###############################################################################################################
#I just have to adapt the previous code of the likelihood to apply it to my reads and replace the 0.25 value
#I'm trying to have a simple function of the likelihood to make it easy to call it

'''
Libra
'''

import numpy as np


'''
Code
'''

#Generate data 
genome = random_dna(3000)
beta = np.random.uniform(0.90,1.00) 
alpha = np.random.random() #Between 0 and 1
print('The random beta value for this run is', beta)
print('The random alpha value for this run is', alpha)
 
l = 100  #read length
N = 1000 #Number of reads
read_list = list()

#This part produce a read, try to mutate the read, N times to produce a list of read based on a reference genome
for yn in range(N) :
    i_n = round(np.random.uniform(0,len(genome)-l))             #a random position in genome (if I had +1 read can have a length of l-1)     
    read = genome[i_n:i_n+l]                                    #a length l read starting on i_n
    r = genome[i_n:i_n+l] 
    read_index = np.random.permutation(l)                       #a list storing permuted nucleotide index
    mut_index = np.arange(round(beta*l))                        #a list of index with a length equal to the possible mutations number
    for m in mut_index :                                        #for each mutation index (m) :
        #print('m value :', m)
        #print('index of the nucleotide to mutate :', read_index[m])
        a = ['A','T','C','G'] ; a.remove(read[read_index[m]])
        yn_mutated = list(read)
        #print('Old nt', yn_mutated[read_index[m]])
        yn_mutated[read_index[m]] = random.choice(a)
        #print('New nt', yn_mutated[read_index[m]])
        #print()
        yn_mutated = ''.join(yn_mutated)
        if np.random.random() < 0.25 : #Here, 25% of mutations are allowed in the read
            read = yn_mutated
    read_list.append(read)
    #print('\ninitial read :', r)
    #print('mutate read :', yn_mutated, '\n')		
    
    

'''
#Fasta file containing produced read with accepted mutations on them
with open ('results/Synthetic-reads_'+str(N)+'-reads-of-'+str(l)+'-nt.fa','w') as out :
	nb = 0
	for r in read_list :
		nb += 1
		print('>read_'+str(nb), r, sep='\n', file=out)
'''


'''
Notes during development
'''

'''
First draft : 

yn_mutated = dict()

for yn in :                                                         #
	sample                                                      #
	permute                                                     #
	l = len(n)                                                  #	Setup the read length
	for m in range(1,beta*l) :                                  #	For X (m) possible mutations in the current read (yn) :
	                                                            #		Choose a random position in the read to mutate this turn
	                                                            
		yn_mutated[m] =                                     #		Compute m mutations to the current read at random places?? (No, cause we have a loop on m, why?)

		if np.random.random() < formula-related-to-LL :     #		Test to decide if we accept or not the currently proposed mutations
			processed_yn = yn_mutated                   #				current mutation is add or not to the read (depending on test results) 
	compute_yn.append(processed_yn )                            #		When all possible mutation tried to happen, save the new read 

==> Have a new generate list of reads with mutations and know beta and alpha.

What are we using to make the first loop ?
How do we produce mutations ? 


Trying to better understand :

Input a genome
for each reads to produce, just take a random genome index and make a read of length l (do we have to fix read length?) starting from this index
for each possible mutation in the current read, applied it to the read, test if we could accept it and replace or not (permutation step ? I know why but not when to do this) check the * at the end
At the end of the mutation loop, we have a read with mutations according with beta and alpha parameters
As we do it for each reads, we have a list of n reads that we can use to test algorithms (which format)


(add permutation somewhere to avoid several mutations at same positions in read, line 44?) I know why but not when


* : first turn, we produce a mutation on the original read, if accepted, this mutate read (m1) replace the original one. For next mutation (m2), we make the test using the new read instead of the orignal one, right ? ( p(m2)/p(m1) and not p(m2)/p(original read) ).

Permutation : 2 vectors per reads
- n : the variable containing the read
- v1 : reads nt index (permutate index of the read)
- v2 : index to mutate
E.g. you have read with indes [1,2,3,4], the first vector would be let's say [4,2,1,3] and the second [1,2]
Therefore you would mutate positions 4 and 2 of the initial read


Old pniy new calculation in the function (bad way to proceed)

    #For each orf (o) in contig (c) :
        #Find the index of the start of the match between contig and orf amino acids
        #For each amino acids of in ORF
            #add to dictionnary the amino acid to the list of ORFs amino acids for a specific position in contig
            
    for o in orf[c] :
        index = math.floor((orf[c][o][2]-yn[c][1])/3) #ORF start on the amino acid sequence of read
        for aa2 in translate(orf[c][o][0]):
            struc[index].append(aa2)
            index += 1 #Represent position of this ORF amino acid on the contig
    
    #When I call this function, I'm at position X on the contig in nucleotide
    #Is there an ORF here
    
    nb=-1
    for aa in translate(ref_genome[yn[c][1]:yn[c][2]]) :
        nb+=1 #Index of amino acid contig position
        print(aa)

'''

