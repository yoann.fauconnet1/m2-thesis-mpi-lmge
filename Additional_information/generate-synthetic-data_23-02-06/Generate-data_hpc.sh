#!/bin/bash
#SBATCH -p medium
#SBATCH -t 6:30:00
#SBATCH -N 40
#SBATCH --ntasks-per-node 24
#SBATCH -o job-%J.out

module load python/3.9.0 anaconda3/2021.05 emboss/6.5.7
source activate datagen-env

python3 scripts/Generate-synthetic-data_23-02-06.py
