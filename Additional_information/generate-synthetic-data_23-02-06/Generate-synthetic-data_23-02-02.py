'''
##Libra#####
'''

import numpy as np ; import re ; import random
from Bio.Align import substitution_matrices ; from collections import defaultdict ; from Bio.Seq import Seq


'''
##Commnents#####
'''

"""
Here, all contigs are forwards, but in a real application, it's not the case so I've to try to adapt this code in order to manage these contigs. I can't just reverse complment reverse cntigs cause we don't know if they're from + or - strand. 
"""


'''
##Tools#####
'''
	
#Translate a given nucleotide sequence into the corresponding amino acid sequence
def translate(seq):
	table = {
		'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
		'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
		'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
		'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',				
		'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
		'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
		'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
		'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
		'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
		'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
		'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
		'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
		'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
		'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
		'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
		'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W',
	}
	protein =""
	if len(seq)%3 == 0:
		for i in range(0, len(seq), 3):
			codon = seq[i:i + 3]
			protein+= table[codon]
	return protein


#Give the DeltaS of each amino acid of a contig compared to the genome 
#(In LL, DeltaS is calculated for the contig between genome in region matching with an ORF)
def calc_delta_S(contig, startg, endg) :
	deltaS = list()
	contig_index = 0
	c_aa = translate(contig)
	#print('aaa', c_aa, contig)
	for a in translate(genome[int(startg):int(endg)]) :                  
		if str(a) == str(c_aa[contig_index]) :
			deltaS.append(float(0.0))
		else:
			deltaS.append(float(blosum[a][c_aa[contig_index]])-float(blosum[a][a])) #-20 penalty for stop codon is already set when importing the blosum matrix	
		contig_index += 1
	return deltaS
	
def calc_delta_S_reverse(contig, startg, endg) :
	deltaS = list()
	contig_index = 0
	c_aa = translate(Seq(contig).reverse_complement())
	for a in translate(Seq(genome[int(startg):int(endg)]).reverse_complement()) : #We reverse the contig sequence so the genome too. No reading frame problem cause ORFs are multiple of 3.                  
		if str(a) == str(c_aa[contig_index]) :            #After this reverse step, calculation may stay the same
			deltaS.append(float(0.0))
		else:
			deltaS.append(float(blosum[a][c_aa[contig_index]])-float(blosum[a][a]))	
		contig_index += 1
	return deltaS

#Calculate the pniy for a specific position xi and nucleotide y
def calc_pniy(xi, y, xi_pos):	
	if xi == y :
		final = 1-beta
	else:
		pniy = beta/3

		prod_DeltaS = 1
		for o in orfs :
			deltas = 0.0
			if orfs[o][0] <= xi_pos <= orfs[o][1] : #Forward ORF
				xo = xi_pos - orfs[o][0]
				orf_seq = genome[orfs[o][0]:orfs[o][1]+1] #+1 or not ?
				temp = orf_seq[:xo]+y+orf_seq[xo+1:] #We're going over all ORF pos matching with xi, we replace the nt in orf by y and calculate deltas with genome
				if   xo%3 == 0 :
					codon_orf = translate(temp[xo:xo+3])
					codon_ref = translate(genome[ (orfs[o][0]+xo) : (orfs[o][0]+xo+3) ])
				elif xo%3 == 1 :
					codon_orf = translate(temp[xo-1:xo+2]) #And not +1 cause the [] is exclusive for the last boy
					codon_ref = translate(genome[ (orfs[o][0]+xo-1) : (orfs[o][0]+xo+2) ])
				elif xo%3 == 2 :
					codon_orf = translate(temp[xo-2:xo+1])
					codon_ref = translate(genome[ (orfs[o][0]+xo-2) : (orfs[o][0]+xo+1) ])

				if codon_orf == '*' :
					deltas = float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref])
					deltas -= 20.0
				elif codon_orf == codon_ref :
					deltas = 0.0	
				else:
					deltas = float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref])
					
			elif orfs[o][1] <= xi_pos <= orfs[o][0] : #Reverse ORF 
				xo = xi_pos - orfs[o][1]
				orf_seq = genome[orfs[o][1]:orfs[o][0]+1]
				tempo = Seq(orf_seq[:xo]+y+orf_seq[xo+1:]).reverse_complement() #We're going over all ORF pos matching with xi, we replace the nt orf by y and calculate dS with genome
				#When we are on reverse, the following code stay the same I guess
				#==> At this step, ORF_seq is the genomic position corresponding to the ORF with xi replaced by y and the sequence is reverse to be translate is the good strand

				if   xo%3 == 0 :
					codon_orf = translate(temp[xo:xo+3])
					rev=genome[ (orfs[o][0]+xo) : (orfs[o][0]+xo+3) ]
				elif xo%3 == 1 :
					codon_orf = translate(temp[xo-1:xo+2]) #And not +1 cause the [] is exclusive for the last boy
					rev = genome[ (orfs[o][0]+xo-1) : (orfs[o][0]+xo+2) ]
				elif xo%3 == 2 :
					codon_orf = translate(temp[xo-2:xo+1])
					rev = genome[ (orfs[o][0]+xo-2) : (orfs[o][0]+xo+1) ]
				codon_ref = translate(Seq(rev).reverse_complement())

				if codon_orf == '*' :
					deltas = float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref])
					deltas -= 20.0
				elif codon_orf == codon_ref :
					deltas = 0.0	
				else:
					deltas = float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref])
				
			exp_deltas = np.exp(alpha*deltas)	
			prod_DeltaS *= exp_deltas
		#print('eee', pniy, prod_DeltaS)
		
		final = pniy*prod_DeltaS
	return final #At the end, if there is no ORF corresponding to this position in the ORF, return 1.			


#This LL use others to produce a results form choosen parameters. When we are going to work with ORFs, espacialy in the last part of the formula, we have to take into consideration ORFs in the reverse strand. Aditionnaly, it is very important to take into account which part of the ORF is on the contig when there is an overlap. This is why there is a lot of if statement in this part of the code. 
#mn = mutation number on the read, read = nucleotidic sequence, i_n = contig start position, l = read length, alpha, beta
def calc_log_likelihood(mn, read, orfs, i_n, l, alpha, beta):
	Pyn = 0
	first = mn*np.log(beta/3)+(len(read)-mn)*np.log(1-beta) #0 mut in the first turn, mn increase if a mut is accepted
	second = 0
	for orf in orfs : #Forward ORFs
		if i_n <= orfs[orf][0] <= i_n+l : ##This line work for forward and reverse (If ORF start is in the read)
			if orfs[orf][0] < orfs[orf][1] : #You're a forward ORF
				c_on_o = read[orfs[orf][0]-i_n:orfs[orf][1]-i_n+1] #Part of the contig matching with current forward ORF when start of orf is on contig
				if len(c_on_o)%3 == 1 :
					c_on_o = c_on_o[:-1]
				elif len(c_on_o)%3 == 2 :
					c_on_o = c_on_o[:-2]
				second += alpha*np.sum(calc_delta_S(c_on_o, orfs[orf][0], orfs[orf][0]+len(c_on_o))) #Sum of ag * Sum of deltaS at each positions
				#print('start on contig')
				#print('ForwardStart')
				#print('read', read[:])
				#print('c_on_o', c_on_o)
				#print('genonme', genome[orfs[orf][0]:orfs[orf][1]+1])
				#print('ForwardEnd')
			else : #You're a reverse ORF
				start = 0 #If lower than 0, its cause the end of the reverse of is before the begening of the contig so we want start at contig 0, contig index -2 doesn't exist.
				if orfs[orf][1]-i_n > 0 :
					start = orfs[orf][1]-i_n
				c_on_ro =  read[start:orfs[orf][0]-i_n+1] #Part of the contig matching with the current reverse ORF when start of orf is on contig
				if len(c_on_ro)%3 == 1 :
					c_on_ro = c_on_ro[:-1]
				elif len(c_on_ro)%3 == 2 :
					c_on_ro = c_on_ro[:-2]
				second += alpha*np.sum(calc_delta_S_reverse(c_on_ro, orfs[orf][1], orfs[orf][1]+len(c_on_ro))) #Sum of ag * Sum of deltaS at each positions
				#print('start on contig')
				#print('ReverseStart')
				#print(c_on_ro)
				#print(genome[orfs[orf][1]:orfs[orf][0]+1])
				#print('ReverseEnd')
		elif i_n <= orfs[orf][1] <= i_n+l : #(if ORF end is in the read and not the start)
			if orfs[orf][0] < orfs[orf][1] : #You're a forward ORF
				c_on_o = read[:orfs[orf][1]-i_n+1] #Part of the contig matching with current forward ORF when end of orf is on contig
				if len(c_on_o)%3 == 1 :
					c_on_o = c_on_o[:-1]
				elif len(c_on_o)%3 == 2 :
					c_on_o = c_on_o[:-2]
				second += alpha*np.sum(calc_delta_S(c_on_o, orfs[orf][0], orfs[orf][0]+len(c_on_o))) #Sum of ag * Sum of deltaS at each positions
				#print('end on contig')
				#print('ForwardStart')
				#print('read', read[:])
				#print('c_on_o', c_on_o)
				#print('genonme', genome[orfs[orf][0]:orfs[orf][1]+1])
				#print('ForwardEnd')
			else : #You're a reverse ORF
				start = 0
				if orfs[orf][1]-i_n > 0 :
					start = orfs[orf][1]-i_n			
				c_on_ro =  read[start:] #Part of the contig matching with the current reverse ORF when end of orf is on contig
				if len(c_on_ro)%3 == 1 :
					c_on_ro = c_on_ro[:-1]
				elif len(c_on_ro)%3 == 2 :
					c_on_ro = c_on_ro[:-2]
				second += alpha*np.sum(calc_delta_S_reverse(c_on_ro, orfs[orf][1], orfs[orf][1]+len(c_on_ro))) #Sum of ag * Sum of deltaS at each positions
				#print('end on contig')
				#print('ReverseStart')
				#print(c_on_ro)
				#print(genome[orfs[orf][1]:orfs[orf][0]+1])		
				#print('ReverseEnd')
	third=0
	xi_pos = i_n
	for xi in read : #first sum after the minus in the formula (Product for each contig of)
		pniy = 0
		li = ['A','T','C','G']
		#print('S')
		for y in li :
			pniy += calc_pniy(xi,y,xi_pos)
			#print('pniy', pniy)
		third += np.log(pniy) #Sum of log(Sum of pniy for 3nt) calculate for each nt of the contig
		xi_pos += 1
		#print('E')
	#print(first, second, third)	
	Pyn = first+second-third
	#print('The log likelihod value is :', Pyn)	
	return Pyn


'''
##Variable Setup#####
'''

#Load the BLOSUM substitution matrix
blosum = substitution_matrices.load('BLOSUM62') #print(blosum['M']['F']) --> Way to call an aa-aa score


#Generate data 
#I use a real genome : Bacillus phage GIL160 and accession : 000859725 to make synthetic datas (there is 3 n and I replace them by A to avoid error, take care of this biais)
#genome = random_dna(3000)
genome = ''
with open ('data/Bacillus-phage-clean.fa','r') as f1 :
	for l in f1 : 
		if not l.startswith('>') :
			l = l.strip().replace('n','A').upper()
			genome += l

#getORF part on the genome to have a dict with ORFname, start and end on the reference genome as a list : ORF = [51, 90]
#First : use the following command in bash --> getorf data/GCA_000859725.1.txt -o data/GCA_000859725.1_ORFs.fa
#In results, we can see ORFs on the reverse side, but let consider the forward ones. 

orfs = defaultdict(list)
with open('data/GCA_000859725.1_ORFs.fa','r') as f1 : 
	for l in f1 : 
		l=l.strip()
		if l.startswith('>'):
			lsplit = l.split()
			orfs[str(lsplit[0].lstrip('>'))] = [int(lsplit[1].lstrip('[')),int(lsplit[3].rstrip(']'))]


beta = np.random.uniform(0.90,1.00) 
alpha = np.random.random() #Between 0 and 1
print('The random beta value for this run is', beta)
print('The random alpha value for this run is', alpha)
 
l = 100  #read length base on Illumina sequencing
N = 1 #Number of reads (small number to be faster)
read_list = list()


'''
Code#####
'''

#This part produce a read, try to mutate the read, N times to produce a list of read based on a reference genome
for yn in range(N) :
	i_n = round(np.random.uniform(0,len(genome)-l))             #a random position in genome (if I had +1 read can have a length of l-1)     
	read = genome[i_n:i_n+l]                                    #a length l read starting on i_n
	r = genome[i_n:i_n+l] 
	read_index = np.random.permutation(l)                       #a list storing permuted nucleotide index
	mut_index = np.arange(round(beta*l))                        #a list of index with a length equal to the possible mutations number
	mn = 0
	for m in mut_index :                                        #for each mutation index (m) :
		mn_mut = mn+1
		#print('m value :', m, 'index of the nucleotide to mutate :', read_index[m])
		a = ['A','T','C','G'] ; a.remove(read[read_index[m]])
		yn_mutated = list(read)
		#print('Old nt', yn_mutated[read_index[m]])
		yn_mutated[read_index[m]] = random.choice(a)
		#print('New nt', yn_mutated[read_index[m]])
		yn_mutated = ''.join(yn_mutated)

		Pyn = calc_log_likelihood(mn, read, orfs, i_n, l, alpha, beta)
		Pyn_mut = calc_log_likelihood(mn_mut, yn_mutated, orfs, i_n, l, alpha, beta)
		print('Final values for this read of Pyn and Pyn_mut', Pyn, Pyn_mut)
		#print(f'On read {yn} and for mutation {m} the ratio is {Pyn_mut/Pyn}')
		
		if np.random.random() < (Pyn_mut/Pyn) : #Here, 25% of mutations are allowed in the read
			mn += 1
			read = yn_mutated
	read_list.append(read)
	print('\ninitial read :', r)
	print('mutate read :', yn_mutated, '\n')		

