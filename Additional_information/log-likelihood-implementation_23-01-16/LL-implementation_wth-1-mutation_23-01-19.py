'''
##Libra#####
'''

import re ; import sys ; import os ; import numpy as np ; import random
from Bio.Align import substitution_matrices ; from collections import defaultdict

'''
##Tools#####
'''

#Give a configurable length reference genome
def random_dna(length):
	return ''.join(random.choice('ATCG') for _ in range(length))
	
#Translate a given nucleotide sequence into the corresponding amino acid sequence
def translate(seq):
	table = {
		'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
		'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
		'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
		'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',				
		'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
		'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
		'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
		'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
		'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
		'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
		'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
		'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
		'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
		'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
		'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
		'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
	}
	protein =""
	if len(seq)%3 == 0:
		for i in range(0, len(seq), 3):
			codon = seq[i:i + 3]
			protein+= table[codon]
	return protein


#Give the DeltaS of each amino acid of a contig compared to the variable ref_genome
def calc_delta_S(contig, startg, endg) :
	deltaS = np.array(float)
	contig_index = 0
	c_aa = translate(contig)
	for a in translate(ref_genome[int(startg):int(endg)]) :                  
		if str(a) == str(c_aa[contig_index]) :
			deltaS = np.append(deltaS, float(0.0))
		else:
			if c_aa[contig_index] == '_': #If the mutation produce a stop codon in this ORF, add -20 
				deltaS = np.append(deltaS, ((float(blosum[a][c_aa[contig_index]])-float(blosum[a][a]))-20) )
			else:
				deltaS = np.append(deltaS, (float(blosum[a][c_aa[contig_index]])-float(blosum[a][a])) )	
		contig_index += 1
	return deltaS


#Give a list of kronecker index for each nucleotide of a contig compared to the variable ref_genome
def calc_kronecker(contig, startg, endg) :
	kronecker = np.array(str)	
	contig_index = 0
	for a in ref_genome[int(startg):int(endg)] :                  
		if str(a) == str(contig[contig_index]) :
			kronecker = np.append(kronecker, str(1))
		else:
			kronecker = np.append(kronecker, str(0))
		contig_index += 1
	return kronecker


#Give kronecker index for xi and y
def kronecker(xi,y):
	if xi==y:
		k=1
	else:
		k=0
	return k


#Compute Product(exp**(alphag*deltaS)) for each position of an ORF (atr) on the contig (c). Give then the product of this per-positions values. (This function use the variable ref_genome)
#It doesn't work if the ORF length is not a multiple of 3
def calc_delta_Spe(c, atr, contig, startg, endg) :
	product = 1.0
	contig_index = 0
	y1_tr = translate(contig)
	for a in translate(ref_genome[int(startg):int(endg)]) : 
		if str(a) == str(y1_tr[contig_index]) :
			product *= 1.0
		else:
			if y1_tr[contig_index] == '_': #If the mutation produce a stop codon in this ORF, add -20 
				product *= np.exp(orf[c][atr][1]*(float(blosum[a][y1_tr[contig_index]])-float(blosum[a][a]))-20)
			else:
				product *= np.exp(orf[c][atr][1]*float(blosum[a][y1_tr[contig_index]])-float(blosum[a][a]))
		contig_index += 1	
	return product


#Give a pniy value for a contig considering each ORF on it
#c for contig name
def calc_pniy(c,xi,y): 
	k = kronecker(xi,y)
	pniy = (bn[c]/3)**(1-k)*(1-bn[c])**k
	
	sum_dg = 1
	#For each ORF in contig c 
	for atr in orf[c] : 
		#Compute Product of Product(exp**(alphag*deltaS)) #The first product give a value per ORF positions so we make another product to get only one value for the ORF
		sum_dg = sum_dg*calc_delta_Spe(c, atr, orf[c][atr][0], orf[c][atr][2], orf[c][atr][3])
	pniy = pniy*sum_dg
	return pniy


'''
##Test data production##### 
'''

#To test the first version of this algorithm, let's create a reference genome, with only one contig and two ORF on it. There is one mutation in the contig and one ORF get it.


#We want to have a static reference genome for now to compare several runs (we can change the number of mutations on the contig for example) so : 
ref_genome = 'TCCTTGGGACAGCTTCTGCGTGACTTTGCACCCAAAGTTCCACGGTCACATGCGCCTGAG'

#We also need a contig, let's take a length of 9 amino acids (27bp) and 1 SNP (at index 10 = C-->T)
yn = dict()
yn['y1'] = 'CAAAGTTCCATGGTCACATGCGCCTGA'

#Two fake ORF on the contig y1 with overlapping positions
#First ORF have a mutation compared to ref_genome but not the second one
orf = defaultdict(lambda: defaultdict(list))
                          #sequence,       ag, start_on_Genome, end_on_Genome
orf['y1']['ORF1'] = ['ACGGTCACATGCGCCTGA', 0.5, 41, 59]
orf['y1']['ORF2'] = ['CATGCGCCTGAG', 2, 48, 60]


#BWA for info about SNP, position in the aligment, ... 
#Be very aware when I'm going to change this into real variable, many change to do


'''
##Variable Setup#####
'''

#Load the BLOSUM substitution matrix
blosum = substitution_matrices.load('BLOSUM62') #print(blosum['M']['F']) --> Way to call an aa-aa score

#Beta and Gamma are unknow values that we want to optimise to have the highest LL as possible, so let's start with false values

#%identity between contig and genome in absence of selection (yes we can calculate the real b_n for contig)
#Only one contig to assess a beta value
bn = dict()	
for c in yn : 
	bn[c] = 0.95 
	
#Penalty when an aa is substitute into a stop codon #Never used, crazy
g = 20             

mn = 1                                       #nb of mutation in the contig relative to the genome (If 1 SNP, this value is equal to 1)
y1_deltaS = calc_delta_S(yn['y1'], 32, 59)   #Only one contig in this script so I run the function on it to obtain deltaS for each of it positions. 	
				              #Normally, we calculate the deltaS on an ORF and not the full contig so be aware of that



'''
##Compute the log likelihood#####
'''

#With classical loops (WARNING : In the formula, ag is for orf and not for contig, in a real case we need to make the sum of these elements and to replace the contig n by all ORFs)


LL = 0
for c in yn : #For each contigs
	first = mn*np.log(bn[c]/3)+(len(yn[c])-mn)*np.log(1-bn[c])
	second = 0
	for atr in orf[c] : #for each ORF in contig c         ORF Sequence    ORF start on g     ORF end on g
		second += (orf[c][atr][1]*np.sum(calc_delta_S(orf[c][atr][0], orf[c][atr][2], orf[c][atr][3])[1:])) #Sum of ag * Sum of deltaS at each positions
	third=0
	for xi in yn[c] : #first sum after the minus in the formula (Product for each contig of)
		pniy=0
		l = ['A','T','C','G']
		l.remove(xi)
		for y in l :
			pniy += calc_pniy(c,xi,y)
		third += np.log(pniy) #Sum of log(Sum of pniy for 3nt) calculate for each nt of the contig
	#contig_likelihood = first+second-third
	LL += first+second-third
print('The log likelihod value is :', LL)


'''

/!\
I want to compute the delta S, so I need to first align the contig to the genome, and know the index of the alignment of the contig on the genome.
With this information I can then calculate the delta S of each position in a contig compared with the genome (0 par default si aucune mutation).  
(Here, I will calculate manualy the value cause idk the shape of input data about snp, orf, ...)
/!\


Some values are equal to 0 so in order to calculate the log and correctly represent the data, I could use a pseudocount, simply by adding 1 to each log values for example to avoid 0. If this transformation is to hard, I can take a lower number such as 0.0001

Instead of calcul_delta, if we have a alignment between contig and genome, I can take SNPs, compute deltaS and sum them to have the good vale for a given contig
Same things for many others variable but this draft allow to run a first implementation of the LL 
'''




















