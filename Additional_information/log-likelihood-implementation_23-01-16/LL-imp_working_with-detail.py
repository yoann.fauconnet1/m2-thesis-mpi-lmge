'''
##Libra#####
'''

import numpy as np ; import random ; import math
from Bio.Align import substitution_matrices ; from collections import defaultdict

'''
##Tools#####
'''

#Give a configurable length reference genome
def random_dna(length):
	return ''.join(random.choice('ATCG') for _ in range(length))
	
#Translate a given nucleotide sequence into the corresponding amino acid sequence
def translate(seq):
	table = {
		'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
		'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
		'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
		'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',				
		'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
		'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
		'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
		'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
		'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
		'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
		'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
		'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
		'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
		'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
		'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
		'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W',
	}
	protein =""
	if len(seq)%3 == 0:
		for i in range(0, len(seq), 3):
			codon = seq[i:i + 3]
			protein+= table[codon]
	return protein


#Give the DeltaS of each amino acid of a contig compared to the variable ref_genome (In LL, DeltaS is calculated for the contig between genome in region matching wth an ORF)
def calc_delta_S(contig, startg, endg) :
	deltaS = list()
	contig_index = 0
	c_aa = translate(contig)
	for a in translate(ref_genome[int(startg):int(endg)]) :                  
		if str(a) == str(c_aa[contig_index]) :
			deltaS.append(float(0.0))
		else:
			if c_aa[contig_index] == '_': #If the mutation produce a stop codon in this ORF, add -20 
				deltaS.append((float(blosum[a][c_aa[contig_index]])-float(blosum[a][a]))-20)
			else:
				deltaS.append(float(blosum[a][c_aa[contig_index]])-float(blosum[a][a]))	
		contig_index += 1
	return deltaS


#Give kronecker index for xi and y
def kronecker(xi,y):
	if xi==y:
		k=1
	else:
		k=0
	return k


def calc_pniy(c, xi, y, xi_pos):
	struc = dict()
	k = kronecker(xi,y)
	pniy = ((bn[c]/3)**(1-k))*(1-bn[c])**k
	
	prod_DeltaS = 1
	for o in orf[c] :
		exp_deltas = 1
		#print('\nORF start on genone :', orf[c][o][2], '\nORF end on genome :', orf[c][o][3], '\nxi position on genome :', xi_pos)
		if orf[c][o][2] <= xi_pos <= orf[c][o][3] :
			prod_DeltaS = 1
			xo = xi_pos - orf[c][o][2]
			temp = orf[c][o][0][:xo]+y+orf[c][o][0][xo+1:] #We going over all ORF pos matching with xi, we replace the nt in orf by y and calculate deltas with genome
			#print(orf[c][o][0][xo])
			#print(temp[xo])
			#print('xo (position in the ORF) :', xo)
			if   xo%3 == 0 :
				codon_orf = translate(temp[xo:xo+3])
				codon_ref = translate(ref_genome[ (orf[c][o][2]+xo) : (orf[c][o][2]+xo+3) ])
			elif xo%3 == 1 :
				codon_orf = translate(temp[xo-1:xo+2]) #And not +1 cause the [] is exclusive for the last boy
				codon_ref = translate(ref_genome[ (orf[c][o][2]+xo-1) : (orf[c][o][2]+xo+2) ])
			elif xo%3 == 2 :
				codon_orf = translate(temp[xo-2:xo+1])
				codon_ref = translate(ref_genome[ (orf[c][o][2]+xo-2) : (orf[c][o][2]+xo+1) ])
			#print(xi, y, codon_orf, codon_ref)	
			#I can calculate deltaS here (for forward ORF only)
			if codon_orf == '*' :
				exp_deltas = np.exp(orf[c][o][1]*(float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref]))-20)
			else:
				exp_deltas = np.exp(orf[c][o][1]*(float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref])))	

		#At xi position, we make the product of exp(a*deltaS) for all concerned ORFs. If xi == y, the product have to be equal to 1
		prod_DeltaS = prod_DeltaS * exp_deltas
		
	final = pniy*prod_DeltaS


	return final #At the end, if there is no ORF correspondinbg to this position in the ORF, return 1.			



#xi is nucleotide, find it position on the genome (start_contig+xi_indexInContig) ==< Give this infos to feed the function
#Iterate over all ORFs : 
	#if start_ORF+end_ORF > 0 :	
		#if start_ORF <= xi_pos <= end_ORF : #Use positions on genome
			#Find ORF_nt position (xo = xi_pos-start_ORF) (index in ORF)
	    		#if round((xo/3)%1*10) == 0 we are on the first reading frame 
				#Traduct the codon xo,xo+1,xo+2
	    			#Traduct ref_genome at this position
	    		#elif round((xo/3)%1*10) == 3 we are on the second reading frame 
	    			#Traduct the codon xo-1,xo,xo+1
	    			#Traduct ref_genome at this position
	    		#elif round((xo/3)%1*10) == 7 we are on the third reading frame
	    			#Traduct the codon xo,xo-1,xo-2
	    			#Traduct ref_genome at this position
	    		#calc_deltaSpe    			
	#else: #We are on on ORF in the reverse strand
		#if end_ORF <= xi_pos <= start_ORF : #Use positions on genome
			#Find ORF_nt position (xo = start_ORF-xi_pos)
	    		#if round((xo/3)%1*10) == 0 we are on the first reading frame 
				#Traduct the codon xo,xo+1,xo+2  #Genomic index are reversed but ORF index are in a regular order, so nothing to change here
	    			#Traduct ref_genome at this position #Take care cause this step is reverse!
	    		#elif round((xo/3)%1*10) == 3 we are on the second reading frame 
	    			#Traduct the codon xo-1,xo,xo+1
	    			#Traduct ref_genome at this position
	    		#elif round((xo/3)%1*10) == 7 we are on the third reading frame
	    			#Traduct the codon xo,xo-1,xo-2
	    			#Traduct ref_genome at this position
	    		#calc_deltaSpe    			
	#Product of exp(a*deltaSpe) for ORFs at this position					


'''
##Test data production##### 
'''

#To test the first version of this algorithm, let's create a reference genome, with only one contig and two ORF on it. There is one mutation in the contig and one ORF get it.


#We want to have a static reference genome for now to compare several runs (we can change the number of mutations on the contig for example) so : 
ref_genome = 'TCCTTGGGACAGCTTCTGCGTGACTTTGCACCCAAAGTTCCACGGTCACATGCGCCTGAG'
                                                       
#We also need a contig, let's take a length of 9 amino acids (27bp) and 1 SNP (at index 10 = C-->T)
yn = dict()
yn['y1'] = ['CAAAGTTCCACGGTCACATGCGCCTGA', 32, 59]

#Two fake ORF
orf = defaultdict(lambda: defaultdict(list))
                          #sequence,       ag, start_on_Genome, end_on_Genome
orf['y1']['ORF1'] = ['ACGGTCACATGCGCCTGA', 0.5, 41, 59]
orf['y1']['ORF2'] = ['CATGCGCCTGAG', 1, 48, 60]

#BWA for info about SNP, position in the aligment, ... 
#Be very aware when I'm going to change this into real variable, many change to do


'''
##Variable Setup#####
'''

#Load the BLOSUM substitution matrix
blosum = substitution_matrices.load('BLOSUM62') #print(blosum['M']['F']) --> Way to call an aa-aa score

#%identity between contig and genome in absence of selection (yes we can calculate the real b_n for contig)
#Only one contig to assess a beta value
bn = dict()	
for c in yn : 
	bn[c] = 0.95 
	
#nb of mutation in the contig relative to the genome (If 1 SNP, this value is equal to 1)
mn = 1


'''
##Compute the log likelihood#####
'''

#With classical loops (WARNING : In the formula, ag is for orf and not for contig, in a real case we need to make the sum of these elements and to replace the contig n by all ORFs)

#print(yn['y1'][0][41-32:41-32+len(orf['y1']['ORF1'][0])])

LL = 0
for c in yn : #For each contigs
	first = mn*np.log(bn[c]/3)+(len(yn[c][0])-mn)*np.log(1-bn[c])
	second = 0
	for atr in orf[c] : #for each ORF in contig c         contig Sequence    ORF start on g     ORF end on g
		c_on_o = yn[c][0][orf[c][atr][2]-yn[c][1]:orf[c][atr][2]-yn[c][1]+len(orf[c][atr][0])] #Part of the contig matching with current ORF
		second += (orf[c][atr][1]*np.sum(calc_delta_S(c_on_o, orf[c][atr][2], orf[c][atr][2]+len(c_on_o)))) #Sum of ag * Sum of deltaS at each positions
	third=0
	xi_pos = yn[c][1]
	for xi in yn[c][0] : #first sum after the minus in the formula (Product for each contig of)
		pniy = 0
		for y in ['A', 'T', 'C', 'G'] :
			pniy += calc_pniy(c,xi,y,xi_pos)
		third += np.log(pniy) #Sum of log(Sum of pniy for 3nt) calculate for each nt of the contig
		xi_pos += 1
	#contig_likelihood = first+second-third
	LL += first+second-third
	print(first, second, third)
print('The log likelihod value is :', LL)

'''
print('\nwork in progress')
new_f('y1','Y','D', 46)
print()

a = new_f('y1','Y','D', 46)
print(a)
'''
