#!/usr/bin/bash

g=$(basename $1 .fa)
seqtk seq -F '#' $2 > $2'.fq'
bowtie2-build $1 data/Reekeekee/$g
bowtie2 --no-unal -p 8 -x data/Reekeekee/$g -U $2'.fq' -S output.sam
