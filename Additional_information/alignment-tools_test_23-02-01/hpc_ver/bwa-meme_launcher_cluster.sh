#!/bin/bash
#SBATCH -p hh
#SBATCH -t 240
#SBATCH -N 1
#SBATCH --ntasks-per-node 24
#SBATCH --mem=50G
#SBATCH -o out/job-%J.out

module load python/3.9.0 anaconda3/2021.05 samtools/1.12 bwa/0.7.12
source activate datagen-env

time sh scripts/bwa-meme_cluster.sh data/rooOceanUs1_genome.fa data/EAM_Yise2_SAG_r.fna results/rooOceanUs1/bwa_rooOceanUs1-EAM_Yise2_SAG_r.fna_23-02-08.sam
