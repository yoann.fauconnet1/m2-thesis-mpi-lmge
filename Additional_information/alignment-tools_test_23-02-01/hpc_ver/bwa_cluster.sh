#!/usr/bin/bash

#genome = $1
#read_list_fa = $2
#bwa results name = $3

bwa index $1
bwa mem -t 24 $1 $2 > BWA-output
