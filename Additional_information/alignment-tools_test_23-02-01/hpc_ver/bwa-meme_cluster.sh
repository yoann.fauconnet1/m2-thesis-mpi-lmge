#!/usr/bin/bash

#genome = $1
#read_list_fa = $2

bwa-meme index $1
bwa-meme mem -t 24 $1 $2 | samtools sort --threads 24 | samtools markdup -r -@ 24 --output-fmt SAM - - | awk '$4!=0' | awk '$12!=\"NM:i:0\"' | grep -v '^@' | cut -f4,6,10,12 > sorted.sam
