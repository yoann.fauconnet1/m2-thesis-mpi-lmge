#!/usr/bin/bash

#genome = $1
#read_list_fa = $2
#bwa results name = $3

bwa index $1
#seqtk seq -F '#' $2 > $2'.fq'
bwa mem -t 8 $1 $2 -o $3
cat $3 | samtools sort --threads 8 -o $3'.sorted.bam'
samtools index -@ 8 $3'.sorted.bam'
samtools tview $3'.sorted.bam' $1



###Test on 1401 synthetic reads from bacillus phage : 

##DATAS
#/home/yoann/Yoann_internship/data/Bacillus-phage-clean.fa
#/home/yoann/Yoann_internship/results/BacillusPhage/a-b-variable_synthetic-reads/synthetic-read-list_1401r-250bp_23-02-08.fa
#/home/yoann/Yoann_internship/results/BacillusPhage/bwa_bacillus-page-1401r-250pb_23-02-08.sam
##COMMAND
#time sh scripts/bwa-wf.sh /home/yoann/Yoann_internship/data/Bacillus-phage-clean.fa /home/yoann/Yoann_internship/results/BacillusPhage/a-b-variable_synthetic-reads/synthetic-read-list_1401r-250bp_23-02-08.fa /home/yoann/Yoann_internship/results/BacillusPhage/bwa_bacillus-page-1401r-250pb_23-02-08.sam



###Test on rooOceanUs1, a reekeekee and the virome from where it come. 3000pb genome and 35M reads on the virome

##DATAS
#/home/yoann/Yoann_internship/data/Reekeekee/rooOceanUs1_genome.fa
#/home/yoann/Yoann_internship/results/rooOceanUs1/EAM_Yise2_SAG_r.fna
#/home/yoann/Yoann_internship/results/rooOceanUs1/bwa_rooOceanUs1-EAM_Yise2_SAG_r.fna_23-02-08.sam
##COMMAND
#time sh scripts/bwa-mem-wf.sh /home/yoann/Yoann_internship/data/Reekeekee/rooOceanUs1_genome.fa /home/yoann/Yoann_internship/results/rooOceanUs1/EAM_Yise2_SAG_r.fna /home/yoann/Yoann_internship/results/rooOceanUs1/bwa_rooOceanUs1-EAM_Yise2_SAG_r.fna_23-02-08.sam
##RUNTIME
#
