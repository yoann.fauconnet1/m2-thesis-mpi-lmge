

'''
#Libra#####
'''

import os ; import re ; import sys ; import argparse
from collections import defaultdict


'''
#Argument parser
'''

pa = argparse.ArgumentParser(description='Total log lkelihood script. Release from 13/02/23.')
pa.add_argument('--genome', '-g',  dest='Genome_to_annotate', help='Fasta file with a full length phage genome')
pa.add_argument('--reads',  '-r',  dest='Reads_to_use',       help='Fasta file containing reads to align on the genome to produce informaton')      
#if no arguments given, show the help message in error output                  
if len(sys.argv)==1:
    pa.print_help(sys.stderr)
    sys.exit(1)             
#object to retrieve arguments with 'dest' (args.dest)           
args     = pa.parse_args() 
genome_f = str(args.Genome_to_annotate)
read_f   = str(args.Reads_to_use)


'''
#Tools#####
'''

#Calculate some information for the current line (This function is here to make the code more readable)
def calc_reads_infos(line) :
	mn      = int(line.split()[2][5:])                         #Number of muation in this read
	r_start = int(line.split()[0])                             #Start position of the read on the reference genome
	length  = int(line.split()[1])                             #Read length
	r_seq   = line.split()[1]                                  #Read sequence

	return mn, r_start, length, r_seq


'''
#Code#####
'''

#Import the genome (in a string structure)
genome = ''
with open (genome_f,'r') as f1 :
	for l in f1 : 
		if not l.startswith('>') :
			genome += l.strip().replace('n','A').upper()

'''
#Run bwa on genome and read file
os.system("bwa-meme index "+genome_f+" ; bwa-meme mem -t 8 "+genome_f+" "+read_f+" \
						| samtools sort --threads 8 \
						| samtools markdup -r -@ 8 --output-fmt SAM - - \
							| awk -v OFS='\t' '$4!=0{print $4, length($10), $12}' \
								> sorted.sam.forpy") #BWA, sort results, remove duplicate, select columns, remove match at position 0, save in a file
'''

#Product ORF file for the current genome (orfipy) and import here (ORF_name = [start, end, alpha_value]) 
os.system("orfipy --procs 8 --pep orfipy_peptide --min 30 --table 21 --ignore-case --outdir "+str(genome_f.split('/')[-1])+"_temp-orf "+genome_f) 
orfs = defaultdict(list)
with open (str(genome_f.split('/')[-1])+'_temp-orf/orfipy_peptide','r') as f1 :
	for l in f1 : 
		if l.startswith('>') :
			orfs[l.split()[0].lstrip('>')] = [re.search(r'\[(\d+)\-\d+\]',l).group(1) , re.search(r'\[\d+\-(\d+)\]',l).group(1)]


#Import every mandatory thing about LL formula and adapt variable to our data

#Import reads with information about start and end on genome, number of SNPs from bwa
	#Calculate the total likelihood for random alpha and beta (or real one cause I have the right values of parameters on my synthtetic reads) just to check if it works
nb=0
go = 0
reads = defaultdict(list)
with open ('sorted.sam.forpy','r') as f1 : 
	for line in f1 : 
		go += 1
		if go > 5 :
			nb+=1
			mn, r_start, l, r_seq = calc_reads_infos(line)
			#reads['read_'+str(nb)] = line.split()

#In this last function, I have everythng I need to computwe the ll execpt the parameters values a,b,g. I need to import tensorflow worflow ? Or just test befor if the program run with false values such as fixed alpha and beta, results are less important in this test than the fact of everything run


#Thinks about gradient 

#As everything is very long, monday, thinks about how to code everything in the with open line by line (so reads by reads in fact).
#In bwa output file, there is a lot of 0 in the result file, maybe it's not aligned sequences that i have to suppress of the analysis
