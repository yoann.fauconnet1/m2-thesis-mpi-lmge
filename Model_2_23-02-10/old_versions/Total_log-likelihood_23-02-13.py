

'''
#Libra#####
'''

import os ; import re ; import sys ; import argparse ; import numpy as np
from Bio.Align import substitution_matrices ; from collections import defaultdict ; from Bio.Seq import Seq ; from joblib import Parallel, delayed


'''
#Argument parser + some args setup
'''

pa = argparse.ArgumentParser(description='Total log lkelihood script. Release from 13/02/23.')
pa.add_argument('--genome', '-g',  dest='Genome_to_annotate', help='Fasta file with a full length phage genome')
pa.add_argument('--reads',  '-r',  dest='Reads_to_use',       help='Fasta file containing reads to align on the genome to produce informaton')      
#if no arguments given, show the help message in error output                  
if len(sys.argv)==1:
    pa.print_help(sys.stderr)
    sys.exit(1)             
#object to retrieve arguments with 'dest' (args.dest)           
args     = pa.parse_args() 
genome_f = str(args.Genome_to_annotate)
read_f   = str(args.Reads_to_use)
blosum = substitution_matrices.load('BLOSUM62')

'''
#Tools#####
'''

#Translate a given nucleotide sequence into the corresponding amino acid sequence
def translate(seq):
	table = {
		'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
		'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
		'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
		'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',				
		'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
		'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
		'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
		'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
		'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
		'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
		'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
		'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
		'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
		'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
		'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
		'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W',
	}
	protein =""
	if len(seq)%3 == 0:
		for i in range(0, len(seq), 3):
			codon = seq[i:i + 3]
			protein+= table[codon]
	return protein


#Give the DeltaS of each amino acid of a contig compared to the genome 
#(In LL, DeltaS is calculated for the contig between genome in region matching with an ORF)
def calc_delta_S(contig, startg, endg) :
	deltaS = list()
	contig_index = 0
	c_aa = translate(contig)
	for a in translate(genome[int(startg):int(endg)]) :                  
		deltaS.append(float(blosum[a][c_aa[contig_index]])-float(blosum[a][a])) #-20 penalty for stop codon is already set when importing the blosum matrix	
		contig_index += 1
	return deltaS

def calc_delta_S_reverse(contig, startg, endg) :
	deltaS = list()
	contig_index = 0
	c_aa = translate(Seq(contig).reverse_complement())
	for a in translate(Seq(genome[int(startg):int(endg)]).reverse_complement()) : #We reverse the read seq so the genome too. No reading frame issue cause ORFs are multiple of 3                  
		deltaS.append(float(blosum[a][c_aa[contig_index]])-float(blosum[a][a]))	
		contig_index += 1
	return deltaS

#Calculate the pniy for a specific position xi and nucleotide y
def calc_pniy(xi, y, xi_pos, beta):	
	final = 1-beta
	if xi != y :
		prod_DeltaS = 1
		for o in orfs :
			deltas = 0.0
			orf_start = orfs[o][0]
			orf_end   = orfs[o][1]

			if orf_start <= xi_pos <= orf_end : #FORWARD ORF
				#FIND REF AND ORF CODON TO COMPARE
				xo = xi_pos - orf_start
				orf_seq = genome[orf_start : orf_end+1] #+1 or not ?
				temp = orf_seq[:xo]+y+orf_seq[xo+1:] #We're going over all ORF pos matching with xi, we replace the nt in orf by y and calculate deltas with genome
				if   xo%3 == 0 :
					codon_orf = translate(temp[xo:xo+3])
					codon_ref = translate(genome[ (orf_start+xo) : (orf_start+xo+3) ])
				elif xo%3 == 1 :
					codon_orf = translate(temp[xo-1:xo+2]) #And not +1 cause the [] is exclusive for the last boy
					codon_ref = translate(genome[ (orf_start+xo-1) : (orf_start+xo+2) ])
				elif xo%3 == 2 :
					codon_orf = translate(temp[xo-2:xo+1])
					codon_ref = translate(genome[ (orf_start+xo-2) : (orf_start+xo+1) ])
				#DELTAS CALCULATION
				deltas = float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref])

			elif orf_end <= xi_pos <= orf_start : #REVERSE ORF
				#FINF REF AND ORF CODON TO COMPARE 
				xo = xi_pos - orf_end
				orf_seq = genome[orf_end : orf_start+1]
				temp = Seq(orf_seq[:xo]+y+orf_seq[xo+1:]).reverse_complement() #We're going over all ORF pos matching with xi, we replace the nt orf by y and calculate dS with genome
				if   xo%3 == 0 :
					codon_orf = translate(temp[xo:xo+3])
					rev = genome[ (orf_start+xo) : (orf_start+xo+3) ]
				elif xo%3 == 1 :
					codon_orf = translate(temp[xo-1:xo+2]) #And not +1 cause the [] is exclusive for the last boy
					rev = genome[ (orf_start+xo-1) : (orf_start+xo+2) ]
				elif xo%3 == 2 :
					codon_orf = translate(temp[xo-2:xo+1])
					rev = genome[ (orf_start+xo-2) : (orf_start+xo+1) ]
				codon_ref = translate(Seq(rev).reverse_complement())
				#DELTAS CALCULATION
				deltas = float(blosum[codon_ref][codon_orf])-float(blosum[codon_ref][codon_ref])

			prod_DeltaS *= np.exp(orfs[o][2]*deltas)
		final = (beta/3)*prod_DeltaS
	return final #At the end, if there is no ORF corresponding to this position in the ORF, return 1.			


#This LL use others to produce a results form choosen parameters. When we are going to work with ORFs, espacialy in the last part of the formula, we have to take into consideration ORFs in the reverse strand. Aditionnaly, it is very important to take into account which part of the ORF is on the contig when there is an overlap. This is why there is a lot of if statement in this part of the code. 
#mn = mutation number on the read, read = nucleotidic sequence, i_n = contig start position, l = read length, beta
def calc_log_likelihood(mn, read, orfs, i_n, l, beta):
	first = mn*np.log(beta/3)+(len(read)-mn)*np.log(1-beta) #0 mut in the first turn, mn increase if a mut is accepted
	second = 0
	Pyn    = 0
	for orf in orfs : #Forward ORFs
		orf_start = orfs[orf][0] #To avoid indexing each time
		orf_end   = orfs[orf][1]
		alpha     = orfs[orf][2]
		
		if i_n <= orf_start <= i_n+l :                                        ##ORF start is in the read and the end can be in or not)
			if orf_start < orf_end : #You're a forward ORF
				c_on_o = read[orf_start-i_n:orf_end-i_n+1]             #Part of the contig matching with current forward ORF when start of orf is on contig
				if len(c_on_o)%3 == 1 :
					c_on_o = c_on_o[:-1]
				elif len(c_on_o)%3 == 2 :
					c_on_o = c_on_o[:-2]
				second += alpha*np.sum(calc_delta_S(c_on_o, orf_start, orf_start+len(c_on_o))) #Sum of ag * Sum of deltaS at each positions

			else :                   #You're a reverse ORF
				c_on_ro =  read[max(0, orf_end-i_n):orf_start-i_n+1]   #Part of the contig matching with the current reverse ORF when start of orf is on contig
				if len(c_on_ro)%3 == 1 :
					c_on_ro = c_on_ro[:-1]
				elif len(c_on_ro)%3 == 2 :
					c_on_ro = c_on_ro[:-2]
				second += alpha*np.sum(calc_delta_S_reverse(c_on_ro, orf_end, orf_end+len(c_on_ro))) #Sum of ag * Sum of deltaS at each positions

		elif i_n <= orf_end <= i_n+l :                                        ##ORF end is in the read and not the start
			if orf_start < orf_end : #You're a forward ORF
				c_on_o = read[:orf_end-i_n+1]                          #Part of the contig matching with current forward ORF when end of orf is on contig
				if len(c_on_o)%3 == 1 :
					c_on_o = c_on_o[:-1]
				elif len(c_on_o)%3 == 2 :
					c_on_o = c_on_o[:-2]
				second += alpha*np.sum(calc_delta_S(c_on_o, orf_start, orf_start+len(c_on_o))) #Sum of ag * Sum of deltaS at each positions

			else :                   #You're a reverse ORF		
				c_on_ro =  read[max(0, orf_end-i_n):] #Part of the contig matching with the current reverse ORF when end of orf is on contig
				if len(c_on_ro)%3 == 1 :
					c_on_ro = c_on_ro[:-1]
				elif len(c_on_ro)%3 == 2 :
					c_on_ro = c_on_ro[:-2]
				second += alpha*np.sum(calc_delta_S_reverse(c_on_ro, orf_end, orf_end+len(c_on_ro))) #Sum of ag * Sum of deltaS at each positions
	third=0
	xi_pos = i_n
	for xi in read : #first sum after the minus in the formula (Product for each contig of)
		pniy = 0
		for y in 'ATCG':
			pniy += calc_pniy(xi,y,xi_pos, beta)
			#print('pniy', pniy)
		third += np.log(pniy) #Sum of log(Sum of pniy for 3nt) calculate for each nt of the contig
		xi_pos += 1
	#print(first, second, third)	
	Pyn = first+second-third
	#print('The log likelihod value is :', Pyn)	
	return Pyn


#Calculate some information for the current line (This function is here to make the code more readable)
def calc_reads_infos(line) :
	mn      = int(line.split()[2][5:])                         #Number of muation in this read
	r_start = int(line.split()[0])                             #Start position of the read on the reference genome
	length  = int(line.split()[1])                             #Read length
	r_seq   = genome[r_start:r_start+int(line.split()[1])]     #Read sequence

	return mn, r_start, length, r_seq


'''
#Code#####
'''

#Import the genome (in a string structure)
genome = ''
with open (genome_f,'r') as f1 :
	for l in f1 : 
		if not l.startswith('>') :
			genome += l.strip().replace('n','A').upper()

'''
#Run bwa on genome and read file
os.system("bwa-meme index "+genome_f+" ; bwa-meme mem -t 8 "+genome_f+" "+read_f+" \
						| samtools sort --threads 8 \
						| samtools markdup -r -@ 8 --output-fmt SAM - - \
							| awk -v OFS='\t' '$4!=0{print $4, length($10), $12}' \
								> sorted.sam.forpy") #BWA, sort results, remove duplicate, select columns, remove match at position 0, save in a file
'''

#Product ORF file for the current genome (orfipy) and import here (ORF_name = [start, end, alpha_value]) 
os.system("orfipy --procs 8 --pep orfipy_peptide --min 30 --table 21 --ignore-case --outdir "+str(genome_f.split('/')[-1])+"_temp-orf "+genome_f) 
orfs = defaultdict(list)
with open (str(genome_f.split('/')[-1])+'_temp-orf/orfipy_peptide','r') as f1 :
	for l in f1 : 
		if l.startswith('>') :
			orfs[l.split()[0].lstrip('>')] = [int(re.search(r'\[(\d+)\-\d+\]',l).group(1)) , int(re.search(r'\[\d+\-(\d+)\]',l).group(1)), 0.8]


#Import every mandatory thing about LL formula and adapt variable to our data

#Import reads with information about start and end on genome, number of SNPs from bwa
	#Calculate the total likelihood for random alpha and beta (or real one cause I have the right values of parameters on my synthtetic reads) just to check if it works
nb=0
go = 0
reads = defaultdict(list)
with open ('sorted.sam.forpy','r') as f1 : 
	for line in f1 : 
		go += 1
		if go > 5 :
			nb+=1
			mn, r_start, l, r_seq = calc_reads_infos(line)
			ll = calc_log_likelihood(mn, r_seq, orfs, r_start, l, 0.95)
			print(ll)
			#reads['read_'+str(nb)] = line.split()


#In this last function, I have everythng I need to computwe the ll execpt the parameters values a,b,g. I need to import tensorflow worflow ? Or just test befor if the program run with false values such as fixed alpha and beta, results are less important in this test than the fact of everything run


#Thinks about gradient 

#As everything is very long, monday, thinks about how to code everything in the with open line by line (so reads by reads in fact).
#In bwa output file, there is a lot of 0 in the result file, maybe it's not aligned sequences that i have to suppress of the analysis
